Feature: Test the attributes of a given element
    As a developer
    I want to be able to test the attributes of a given element

    Background:
        Given I open the site "/"

    Scenario: The attribute "href" of a signup button should be "/signup"
        Then  I expect that the attribute "href" from element "#signup" is "/signup"
