Feature: Test Search feature
    As a developer
    I want to be able to test if the search works

    Background:
        

    Scenario: Test the search input
        Given I open the site "/"
        Given I expect that element "#search" contains the text "Search"
        When  I set "hello" to the inputfield "//input[@type='search']"
        Then  I expect that element "//input[@type='search']" contains the text "hello"
        When I click on the button "//button[@type='submit']"
        Then I expect that element "//h3[text()='hello']" becomes displayed
