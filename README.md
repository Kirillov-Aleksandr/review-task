# reviewTask
WebdriverIO + Cucumber + Selenium-standalone + Allure
# Framework installation

Go to the webdriverIO folder:

cd webdriverIO

Install the packages:

yarn
# Running the tests locally

From the webdriverIO folder do the following command, which runs the tests:

yarn run wdio

If you want to watch the test report you could run:

yarn run report:local
# Running the tests on Gitlab

Tests run in Gitlab pipeline on repository push. You can watch the latest test run report on https://kirillov-aleksandr.gitlab.io/review-task/ page
